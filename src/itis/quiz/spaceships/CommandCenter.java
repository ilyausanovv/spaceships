package itis.quiz.spaceships;

import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {

        int maxPowerFul = 0;
        Spaceship mostPowerFulShip = null;
        for (Spaceship spaceship : ships) {
            if (spaceship.getFirePower() > maxPowerFul && spaceship.getFirePower() != 0) {
                maxPowerFul = spaceship.getFirePower();
                mostPowerFulShip = spaceship;
            }
        }
        return mostPowerFulShip;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {

        Spaceship nameShip = null;
        for (int i = 0; i < ships.size(); i++) {
            if(ships.get(i).getName() == name){
                nameShip = ships.get(i);
            }

        }
        return nameShip;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {

        ArrayList<Spaceship> chargeEnoughCargoSpace = new ArrayList<>();
        for (int i = 0; i < ships.size(); i++) {
            if(ships.get(i).getCargoSpace() >= cargoSize){
                chargeEnoughCargoSpace.add(ships.get(i));
            }
        }
        return chargeEnoughCargoSpace;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {

        ArrayList<Spaceship> peacefulShips = new ArrayList<>();
        for (int i = 0; i < ships.size(); i++) {
            if(ships.get(i).getFirePower() == 0){
                peacefulShips.add(ships.get(i));
            }
        }
        return peacefulShips;
    }
}
