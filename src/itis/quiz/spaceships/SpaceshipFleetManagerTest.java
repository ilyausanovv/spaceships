package itis.quiz.spaceships;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class SpaceshipFleetManagerTest {

    ArrayList<Spaceship> testShip = new ArrayList<>();
    SpaceshipFleetManager manager;

    public static void main(String[] args) {

        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());
        boolean testOne = spaceshipFleetManagerTest.getShipName_shipExist_desiredShip();

        if (testOne) {
            System.out.println("The test one was passed!");
        } else {
            System.out.println("The test one was failed!");
        }

        boolean testTwo = spaceshipFleetManagerTest.getShipName_shipNotExist_null();

        if (testTwo) {
            System.out.println("The test two was passed!");
        } else {
            System.out.println("The test two was failed!");
        }

        boolean testThree = spaceshipFleetManagerTest.getMostPowerfulShip_shipExist_desiredShip();

        if (testThree) {
            System.out.println("The test three was passed!");
        } else {
            System.out.println("The test three was failed!");
        }

        boolean testFour = spaceshipFleetManagerTest.getMostPowerfulShip_shipNotExist_null();

        if (testFour) {
            System.out.println("The test four was passed!");
        } else {
            System.out.println("The test four was failed!");
        }

        boolean testFive = spaceshipFleetManagerTest.getMostPowerfulShip_shipExist_firstShip();

        if(testFive){
            System.out.println("The test five was passed!");
        } else {
            System.out.println("The test five was failed!");
        }

        boolean testSix = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipExist_desiredShip();

        if(testSix){
            System.out.println("The test six was passed!");
        } else {
            System.out.println("The test six was failed!");
        }

        boolean testSeven = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipExist_null();

        if(testSeven){
            System.out.println("The test seven was failed!");
        } else {
            System.out.println("The test seven was passed!");
        }

        boolean testEight = spaceshipFleetManagerTest.getAllCivilianShips_shipExist_desiredShip();

        if(testEight){
            System.out.println("The test eight was passed!");
        } else {
            System.out.println("The test eight was failed!");
        }

        boolean testNine = spaceshipFleetManagerTest.getAllCivilianShips_shipNotExist_null();

        if(testNine){
            System.out.println("The test nine was passed!");
        } else {
            System.out.println("The test nine was failed!");
        }

    } 

    private boolean getShipName_shipExist_desiredShip() {

        String name = "Normandy";
        testShip.add(new Spaceship("Normandy", 60, 30, 75));
        testShip.add(new Spaceship("Graal", 90, 15, 50));
        testShip.add(new Spaceship("Peace", 15, 80, 85));
        Spaceship test = manager.getShipByName(testShip, name);
        if (test != null && test.getName().equals(name)) {
            testShip.clear();
            return true;
        }
        testShip.clear();
        return false;
    }

    private boolean getShipName_shipNotExist_null() {

        String name = "Unknown";
        testShip.add(new Spaceship("Normandy", 60, 30, 75));
        testShip.add(new Spaceship("Graal", 90, 15, 50));
        testShip.add(new Spaceship("Peace", 15, 80, 85));
        Spaceship test = manager.getShipByName(testShip, name);
        if (test == null) {
            testShip.clear();
            return true;
        }
        testShip.clear();
        return false;
    }

    private boolean getMostPowerfulShip_shipExist_desiredShip() {

        testShip.add(new Spaceship("Normandy", 60, 30, 75));
        testShip.add(new Spaceship("Graal", 90, 15, 50));
        testShip.add(new Spaceship("Peace", 15, 80, 85));
        Spaceship test = manager.getMostPowerfulShip(testShip);
        Spaceship ship = testShip.get(0);
        for (int i = 0; i < testShip.size(); i++) {
            if (testShip.get(i).getFirePower() > ship.getFirePower()) {
                ship = testShip.get(i);
            }
        }
        if (test == ship && test != null) {
            testShip.clear();
            return true;
        }
        testShip.clear();
        return false;
    }

    private boolean getMostPowerfulShip_shipNotExist_null() {

        testShip.add(new Spaceship("Normandy", 0, 30, 75));
        testShip.add(new Spaceship("Graal", 0, 15, 50));
        testShip.add(new Spaceship("Peace", 0, 80, 85));
        Spaceship test = manager.getMostPowerfulShip(testShip);
        if (test == null) {
            testShip.clear();
            return true;

        }
        testShip.clear();
        return false;
    }

    private boolean getMostPowerfulShip_shipExist_firstShip() {

        testShip.add(new Spaceship("Normandy", 60, 30, 75));
        testShip.add(new Spaceship("Graal", 90, 15, 50));
        testShip.add(new Spaceship("Peace", 15, 80, 85));
        Spaceship test = manager.getMostPowerfulShip(testShip);
        Spaceship ship = testShip.get(0);
        for (int i = 0; i < testShip.size(); i++) {
            if (testShip.get(i).getFirePower() > ship.getFirePower()) {
                ship = testShip.get(i);
            }
        }
        if (test == ship && test != null) {
            testShip.clear();
            return true;
        }
        testShip.clear();
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_shipExist_desiredShip(){

        ArrayList<Spaceship> cargoShips;
        int cargoSize = 80;
        testShip.add(new Spaceship("Normandy", 60, 30, 75));
        testShip.add(new Spaceship("Graal", 90, 15, 50));
        testShip.add(new Spaceship("Peace", 15, 80, 85));
        cargoShips = manager.getAllShipsWithEnoughCargoSpace(testShip, cargoSize);
        for (int i = 0; i < cargoShips.size(); i++) {
            if(cargoShips.get(i).getCargoSpace() < cargoSize){
                testShip.clear();
                return false;
            }
        }
        testShip.clear();
        return true;
    }

    private boolean getAllShipsWithEnoughCargoSpace_shipExist_null(){

        ArrayList<Spaceship> cargoShips;
        int cargoSize = 100;
        testShip.add(new Spaceship("Normandy", 60, 0, 75));
        testShip.add(new Spaceship("Graal", 90, 0, 50));
        testShip.add(new Spaceship("Peace", 15, 80, 85));
        cargoShips = manager.getAllShipsWithEnoughCargoSpace(testShip, cargoSize);
        for (int i = 0; i < cargoShips.size(); i++) {
            if(cargoShips.get(i).getCargoSpace() < cargoSize){
                testShip.clear();
                return true;
            }
        }
        testShip.clear();
        return false;
    }

    private boolean getAllCivilianShips_shipExist_desiredShip(){

        ArrayList<Spaceship> civilianShips;
        testShip.add(new Spaceship("Normandy", 60, 0, 75));
        testShip.add(new Spaceship("Graal", 90, 0, 50));
        testShip.add(new Spaceship("Peace", 15, 80, 85));
        civilianShips = manager.getAllCivilianShips(testShip);
        for (int i = 0; i < civilianShips.size(); i++) {
            if(civilianShips.get(i).getFirePower() != 0){
                testShip.clear();
                return false;
            }
        }
        return true;
    }

    private boolean getAllCivilianShips_shipNotExist_null(){

        ArrayList<Spaceship> civilianShips;
        testShip.add(new Spaceship("Normandy", 0, 0, 15));
        testShip.add(new Spaceship("Graal", 0, 25, 55));
        testShip.add(new Spaceship("Peace", 0, 15, 34));
        civilianShips = manager.getAllCivilianShips(testShip);
        for (int i = 0; i < civilianShips.size(); i++) {
            if(civilianShips == null){
                testShip.clear();
                return false;
            }
        }
        return true;
    }



    public SpaceshipFleetManagerTest(SpaceshipFleetManager manager) {
        this.manager = manager;
    }
}
